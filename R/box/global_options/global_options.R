## --- --- --- --- --- ---
#
# Purpose: Global options for the main script
#
# Author: Michael Miles
#
# Date Created: 2023-04-01
#  
## --- --- --- --- --- ---


# Global options function ------------------------------------------------------

# Sets the global options for use in the main script

#' @export
set_global_options <- function() {
  
  options(
    
    # Set local directories
    input = 'local/data/',
    output = 'local/output/'
    
  )
  
}