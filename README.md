# healthverity r demo

## About

This script is for the healthverity Data QA Engineer Assessment. Raw project data is stored in local/data. Output data is stored in local/output. This script is in addition to the python script and shows how I would approach the same questions using R.

## Running the script

To run the script, open healthverity-r-demo.Rproj . Open the scripts/main.R and click "source" to run the script. Click through any prompts to download necessary packages. If asked, type 'y' and press enter for hydrating the package functions.

## Project structure

All code is stored in the scripts folder. Code is divide into the main script (main.R) and submodules that are found in R/box/. Sub-modules include:

-   global_options/global_options.R (Sets global options for the script)

-   io/import.R (Functions for loading data)

-   io/export.R (Functions for saving data)

-   mod/mod.R (Functions for modifying data)

-   question_funs/question_funs.R (Functions for answering the assessment questions)

These sub modules contain functions that are loaded in the main script and used to conveniently wrap complex code into easy to understand function names.

## Output

Output is stored in the local/output folder:

-   question_1.csv

-   question_2.csv

-   question_3.csv

The csv files for question one and question two directly answer the first two questions. question_3.csv has columns that flag any errors. These columns allow the user to filter for a particular error, or for error-free data.
